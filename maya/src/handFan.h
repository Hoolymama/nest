#ifndef _handFan
#define _handFan


 #if defined(linux)
#ifndef LINUX
 #define LINUX
#endif
 #endif

#if defined(OSMac_MachO_)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <maya/MBoundingBox.h>
#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 
#include <maya/MPxLocatorNode.h>
 
class handFan : public MPxLocatorNode
{
public:
						handFan();
	virtual				~handFan(); 

	virtual MStatus		compute( const MPlug& plug, MDataBlock& data );

	static  void*		creator();
	static  MStatus		initialize();
	
	
	// functions that must be overridden in a locator
	virtual void            draw( M3dView & view, const MDagPath & path,
								  M3dView::DisplayStyle style,
								  M3dView::DisplayStatus status );

	virtual bool            isBounded() const;
	virtual MBoundingBox    boundingBox() const; 
public:

	static  MObject aPoint1;
	static  MObject	aPoint2;        
    static  MObject aOrigin;        
	static  MObject	aBlender;      
    static  MObject	aLength;   
    static  MObject	aMult;       
	static  MObject	aColor1;
	static  MObject	aColor2; 
	static  MObject	aShowLines; 
	static  MObject	aShowIndices;  
	static  MObject	aOutPoint;   
	static	MTypeId	id;
};

#endif
