

#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>

#include "errorMacros.h"


#include "handFan.h"





MStatus initializePlugin( MObject obj )
{
	
	MStatus st;
	
	MString method("initializePlugin");
	
	 MFnPlugin plugin( obj, PLUGIN_VENDOR, PLUGIN_VERSION , MAYA_VERSION);

	st = plugin.registerNode( "handFanShape", handFan::id, handFan::creator,handFan::initialize, MPxNode::kLocatorNode  );ert;

		MGlobal::executePythonCommand("import nest;nest.load()");

	return st;
	
}

MStatus uninitializePlugin( MObject obj)
{
	MStatus st;
	
	MString method("uninitializePlugin");
	
	MFnPlugin plugin( obj );

	st = plugin.deregisterNode( handFan::id );ert;

	return st;
}



