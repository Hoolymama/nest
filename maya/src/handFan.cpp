//
// Copyright (C) 2001 hoolyMama 
// 
// File: handFan.cpp
//
// Dependency Graph Node: handFan
//
// Author: Maya SDK Wizard
//
#include <maya/MIOStream.h>

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MVector.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatVectorArray.h>

#include <maya/MArrayDataBuilder.h>
#include <maya/MColorArray.h>
#include <maya/MFloatArray.h>
#include <maya/MQuaternion.h>
#include <maya/MEulerRotation.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MPoint.h>
#include <maya/MBoundingBox.h>
#include "errorMacros.h"
#include "jMayaIds.h"
#include "handFan.h"


#include <maya/MGlobal.h>

// unique number for this plugin
//MTypeId     handFan::id( 0x8009875 );
MTypeId     handFan::id( k_handFan );


// // attributes we are adding to this node
MObject     handFan::aPoint1;
MObject     handFan::aPoint2;        
MObject     handFan::aOrigin;        
MObject     handFan::aBlender;   
MObject     handFan::aLength;   
MObject     handFan::aMult;
MObject     handFan::aColor1;
MObject     handFan::aColor2; 
MObject     handFan::aShowLines;   
MObject     handFan::aShowIndices;  
MObject     handFan::aOutPoint;



handFan::handFan() {}
handFan::~handFan() {}



MStatus handFan::compute( const MPlug& plug, MDataBlock& data )
{

	MStatus st;

	if ( !(plug == aOutPoint)  ) return MS::kUnknownParameter;


	// we need the same input index as that of the requested output
	int multiIndex = plug.logicalIndex( &st );er;
	MArrayDataHandle hBlenderArray = data.inputArrayValue( aBlender, &st );er;
	// if the ;corresponding input is not there, st will be set to MS::kFailure and 
	// maya will crash if we continue. So bail out here if that's the case.
	st = hBlenderArray.jumpToElement( multiIndex );
	if (st.error())  {
		MString err = "No element at blender ";
		err +=multiIndex;
		MGlobal::displayError(err );
		return MS::kFailure;
	}
	float lengthMult = data.inputValue( aMult).asFloat() ;
	float blender = hBlenderArray.inputValue( ).asFloat();
	double reverseBlender = 1.0 - blender;

	//  how to calc the length
	// 0 = normalize it
	// 1 = point1 length
	// 2 = point2 length
	// 3 = blend between input lengths
	short lengthCalc = data.inputValue( aLength).asShort() ;

	const MVector& p1 = data.inputValue( aPoint1 ).asFloatVector();
	const MVector& p2 = data.inputValue( aPoint2 ).asFloatVector();
	const MVector& origin = data.inputValue( aOrigin ).asFloatVector();



	MArrayDataHandle hOutArray = data.outputArrayValue( aOutPoint, &st);er;
	MArrayDataBuilder bOutArray = hOutArray.builder( &st);er;
	MDataHandle hOut = bOutArray.addElement(multiIndex, &st);er;
	MFloatVector & result = hOut.asFloatVector();
	MVector v1 = MVector(p1 - origin);
	MVector v2 = MVector(p2 - origin);


	//cerr << "p1: " << p1 << " p2: "<< p2 << endl;
	//cerr << "v1: " << v1 << " v2: "<< v2 << endl;

	MVector tmp;
	if (   (v1.isEquivalent(MVector::zero)) ||  (v2.isEquivalent(MVector::zero)) ){
		if (lengthCalc <= 1) {
			tmp = p1;
		} else 	if (lengthCalc == 2) {
			tmp = p2;
		} else  { // lengthCalc == 3
			tmp = origin + (v1 *   ( (v1.length() *reverseBlender  ) +  (v2.length() * blender)) * lengthMult  );
		}
	} else {
		MQuaternion q(v1,v2,blender);

		if (lengthCalc == 0) {
			tmp = origin +( v1.rotateBy(q).normal()* lengthMult);
		} else 	if (lengthCalc == 1) {
			tmp = origin + (v1.rotateBy(q)* lengthMult);
		} else 	if (lengthCalc == 2) {
			tmp = origin + ( v1.rotateBy(q).normal() * v2.length()* lengthMult);
		} else 	 { // lengthCalc == 3
			tmp = origin + (v1.rotateBy(q).normal() * ( (v1.length() *reverseBlender  ) +  (v2.length() * blender))* lengthMult);
		} 
	}

	result = MFloatVector(tmp);

	data.setClean(plug);

	return MS::kSuccess;
}


void handFan::draw( M3dView & view, const MDagPath & path, 
	M3dView::DisplayStyle style,
	M3dView::DisplayStatus status )
{ 
//	cerr << "draw 1" << endl;
	MStatus stat;

	MObject thisNode = thisMObject();	
	MFloatVector p1, p2, origin;
	MColor c1, c2;
	float mult;
	short lengthCalc;
	bool showLines;
	bool showIndices;
	MPlug plug( thisNode, aMult );
	plug.getValue( mult );	

	plug.setAttribute(aShowLines);
	plug.getValue( showLines );

	plug.setAttribute(aShowIndices);
	plug.getValue( showIndices );

	plug.setAttribute(aLength);
	plug.getValue( lengthCalc );

	plug.setAttribute(aPoint1);
	plug.child(0).getValue( p1[0] );	
	plug.child(1).getValue( p1[1] );	
	plug.child(2).getValue( p1[2] );	

	plug.setAttribute(aPoint2);
	plug.child(0).getValue( p2[0] );	
	plug.child(1).getValue( p2[1] );	
	plug.child(2).getValue( p2[2] );

	plug.setAttribute(aOrigin);
	plug.child(0).getValue( origin[0] );	
	plug.child(1).getValue( origin[1] );	
	plug.child(2).getValue( origin[2] );

	MVector v1 = MVector(p1 - origin);
	MVector v2 = MVector(p2 - origin);

	// not much point in drawing anything if the points are on top of each other or on the ame line from the origin
	if (  
		v1.isEquivalent(MVector::zero) ||  
		v2.isEquivalent(MVector::zero) ||
		v1.isEquivalent(v2)  || 
		(! (showLines || showIndices))
		) return;

	plug.setAttribute(aColor1);
	plug.child(0).getValue(  c1[0] );	
	plug.child(1).getValue(  c1[1] );	
	plug.child(2).getValue(  c1[2] );

	plug.setAttribute(aColor2);
	plug.child(0).getValue(  c2[0] );	
	plug.child(1).getValue(  c2[1] );	
	plug.child(2).getValue(  c2[2] );

	MFloatArray blender;
	MPlug blenderPlug( thisNode, aBlender ); 
	int n = blenderPlug.numElements();
	//cerr << "Num Elements: " << n << endl;	
	//MFloatArray blender(n);

	MFloatVectorArray points;
	MColorArray colors;
	MIntArray indices;
//	cerr << "draw 2" << endl;

	for (int i = 0;i< n;i++) {
		MPlug elPlug = blenderPlug.elementByPhysicalIndex(i);
		indices.append(elPlug.logicalIndex());
		float blender;
		elPlug.getValue(blender);
		float reverseBlender = 1.0f - blender;
		MQuaternion q(v1,v2,blender);
		colors.append( (c1*reverseBlender) + (c2 * blender) );

		if (lengthCalc == 0) {
			points.append(origin +( v1.rotateBy(q).normal()* mult));
		} else 	if (lengthCalc == 1) {
			points.append(origin + (v1.rotateBy(q)* mult));
		} else 	if (lengthCalc == 2) {
			points.append(origin + ( v1.rotateBy(q).normal() * v2.length()* mult));
		} else 	 { // lengthCalc == 3
			points.append(origin + (v1.rotateBy(q).normal() * ( (v1.length() *reverseBlender  ) +  (v2.length() * blender))* mult));
		} 

	}
//	cerr << "draw 3" << endl;

	view.beginGL(); 
	//bool viewActive = ( status == M3dView::kActive ) ;
//	if (viewActive) view.setDrawColor( 13, M3dView::kActiveColors );
	//if (! viewActive ) view.setDrawColor( MColor(1.0f,0.0f,0.0f));
//	cerr << "showIndices " << showIndices << endl;

	if (showLines) {
		glBegin( GL_LINES );
		for(int i = 0; i < n; ++i)
		{
		//	if (! viewActive ) view.setDrawColor( colors[i] );
			view.setDrawColor( colors[i] );

		// 	 if (! viewActive ) glColor( colors[i].r,  colors[i].g, colors[i].b);
		// 	cerr << "O: " << origin << " P: "<< points[i] << endl;

			glVertex3f( origin.x , origin.y , origin.z );
			glVertex3f(	points[i].x , points[i].y, points[i].z);


		}
		glEnd();
	}	
	
	if (showIndices){ 
		for(int i = 0; i < n; ++i)
		{
			MString si; si += indices[i];
			view.setDrawColor( colors[i] );
			view.drawText(si, MPoint(points[i]), M3dView::kLeft);
		}
	}
	
//	cerr << "glEnd(  )" << endl;

	view.endGL();
}


void* handFan::creator()

{
	return new handFan();
}

bool handFan::isBounded() const
{ 
	return true;
}


MBoundingBox handFan::boundingBox() const
{   
	//cerr << " in boundingBox" << endl;


	MObject thisNode = thisMObject();	
	MFloatVector p1, p2, origin;
	MColor c1, c2;
	float mult;
	short lengthCalc;
	bool showLines = false;
	bool showIndices;

	MPlug plug( thisNode, aMult );
	plug.getValue( mult );	



	plug.setAttribute(aShowLines);
	plug.getValue( showLines );
	
	plug.setAttribute(aShowIndices);
	plug.getValue( showIndices );

	plug.setAttribute(aLength);
	plug.getValue( lengthCalc );

	plug.setAttribute(aPoint1);
	plug.child(0).getValue( p1[0] );	
	plug.child(1).getValue( p1[1] );	
	plug.child(2).getValue( p1[2] );	

	plug.setAttribute(aPoint2);
	plug.child(0).getValue( p2[0] );	
	plug.child(1).getValue( p2[1] );	
	plug.child(2).getValue( p2[2] );

	plug.setAttribute(aOrigin);
	plug.child(0).getValue( origin[0] );	
	plug.child(1).getValue( origin[1] );	
	plug.child(2).getValue( origin[2] );

	MVector v1 = MVector(p1 - origin);
	MVector v2 = MVector(p2 - origin);


	if (! (showLines || showIndices)) return MBoundingBox();
	// not much point in drawing anything if the points are on top of each other
	if (  
		v1.isEquivalent(MVector::zero) ||  
		v2.isEquivalent(MVector::zero) ||
		v1.isEquivalent(v2) 
		) 
	{
		MBoundingBox resultBox;
		//MPoint pOrig(origin);
		resultBox.expand(MPoint(v1));
		resultBox.expand(MPoint(v2));
		resultBox.expand(MPoint(origin));
		//resultBox.expand(pOrig);
	//	return resultBox;
	//	return MBoundingBox( MPoint(v1), MPoint(v2) ).expand(pOrig);;
		return resultBox;
	}



	float maxLength;

	if (lengthCalc == 0) {
		maxLength = mult;
	} else 	if (lengthCalc == 1) {
		maxLength = mult * v1.length();
	} else 	if (lengthCalc == 2) {
		maxLength = mult * v2.length();
	} else 	 { // lengthCalc == 3
		maxLength =  v2.length();
		if (maxLength < v1.length()) maxLength = v1.length();
		maxLength *= mult;
	} 
	MPoint minCorner(MPoint(origin) - MVector(maxLength,maxLength,maxLength));
	MPoint maxCorner(MPoint(origin) + MVector(maxLength,maxLength,maxLength));

	MBoundingBox result(minCorner, maxCorner);

	return result;

}

MStatus handFan::initialize()
{

	MStatus	st;
	MString method("handFan::initialize");
	MFnNumericAttribute nAttr;
	MFnEnumAttribute eAttr;


	aLength = eAttr.create( "lengthCalc", "lc", 0);
	eAttr.addField("normalize", 0);
	eAttr.addField("point1", 1);
	eAttr.addField("point2", 2);
	eAttr.addField("blend", 3);
	eAttr.setKeyable(true);
	eAttr.setHidden(false);
	st = addAttribute( aLength );er;

	aMult  = nAttr.create( "lengthMult", "lm", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	nAttr.setDefault(1.0f);
	st = addAttribute( aMult );er;


	aPoint1= nAttr.createPoint( "point1", "p1" );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	st = addAttribute( aPoint1 );er;

	aPoint2= nAttr.createPoint( "point2", "p2" );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	st = addAttribute( aPoint2 );er;

	aOrigin= nAttr.createPoint( "origin", "or" );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	st = addAttribute( aOrigin );er;


	aBlender  = nAttr.create( "blender", "b", MFnNumericData::kFloat);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	nAttr.setArray(true);
	st = addAttribute( aBlender );er;


	// diagnostics
	aColor1= nAttr.createColor( "color1", "c1" );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	st = addAttribute( aColor1 );er;


	aColor2= nAttr.createColor( "color2", "c2" );
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	st = addAttribute( aColor2 );er;


	aShowLines  = nAttr.create( "showLines", "sl", MFnNumericData::kBoolean);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	st = addAttribute( aShowLines );er;

	aShowIndices  = nAttr.create( "showIndices", "si", MFnNumericData::kBoolean);
	nAttr.setKeyable(true);
	nAttr.setStorable(true);
	nAttr.setReadable(false);
	nAttr.setWritable(true);
	nAttr.setHidden(false);
	st = addAttribute( aShowIndices );er;


	aOutPoint= nAttr.createPoint( "outPoint", "op" );
	nAttr.setStorable(false);
	nAttr.setReadable(true);
	nAttr.setHidden(false);
	nAttr.setWritable(false);
	nAttr.setArray(true);
	nAttr.setUsesArrayDataBuilder(true); 
	st = addAttribute( aOutPoint );er;

	st = attributeAffects( aPoint1, aOutPoint );er;
	st = attributeAffects( aPoint2, aOutPoint );er;
	st = attributeAffects( aOrigin, aOutPoint );er;
	st = attributeAffects( aBlender, aOutPoint );er;
	st = attributeAffects( aLength, aOutPoint );er;
	st = attributeAffects( aMult, aOutPoint );er;

	return MS::kSuccess;

}




